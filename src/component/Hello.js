import React,{Component}  from 'react'

class Hello extends Component {
    state = {
        name : ''
    }

    inputHenldel = (event) => {
        this.setState({
            name : event.target.value
        })
        console.log(event.target.value)
    }
    render() {
        return (
            <div>
                <input name="name" type="text" value={ this.state.name } onChange={ this.inputHenldel }
                placeholder="Enter Your Name" />
                
                { this.state.name ? <p>Hello { this.state.name }</p> : '' }
            </div>
        ) 
    }
}


export default Hello
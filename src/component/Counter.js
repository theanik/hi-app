import React from 'react'

class Counter extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            count : 10,
            color : 'black'
        }
    }

    decrement = () => {
        this.setState({
            count : this.state.count - 1
        })

        if(this.state.count <= 5){
            this.setState({
                color : 'red'
            })
        }else if(this.state.count >= 15){
            this.setState({
                color : 'green'
            })
        }else{
            this.setState({
                color : 'black'
            })
        }
    }

    inc = () => {
        this.setState({
            count : this.state.count + 1
        })
        
        if(this.state.count < 5){
            this.setState({
                color : 'red'
            })
        }else if(this.state.count > 15){
            this.setState({
                color : 'green'
            })
        }else{
            this.setState({
                color : 'black'
            })
        }
    }
    render(){
        return(
            <div>
                <h1 style={{ color : this.state.color }}> <span onClick={ this.decrement } > - </span> { this.state.count } <span onClick={ this.inc }> + </span> </h1>
            </div>
        )
    }
}

export default Counter
import React from 'react';


class Boom extends React.Component {
  state = {
    persons : [
      {name:"Anik", email : "anik@anw.aa"},
      {name:"Mona", email : "mona@anw.aa"}
    ],
    display : 'none'

  }

  fun1 = () => {
    this.setState({
      display : true
    })
    console.log("OKKK")
  }
  fun2 = () => {
    this.setState({
      display : 'none'
    })
    console.log("OKKK")
  }
  render(){
        return (
          <div className="Boom">
            {/* this is exaple for passing props data */}
          {/* <First name="Anik Anwar"  email="anik@aa.aa"></First>
          <First name="Brin Adams"  email="anik@bb.aa"></First> */}

          {/* {this.state.persons.map( (item, index) => {
            return <First key={index} name={item.name} email={ item.email} ></First>
          }) } */}

          {/* <Counter></Counter> */}


          <button onClick={this.state.display == 'none' ? this.fun1 : this.fun2}>
            BOOM
          </button>

          <h1 style={{display : this.state.display }}>BOOOOOOOOM</h1>
            
          </div>
      );
  }

}

export default Boom;

import React,{Component}  from 'react'

class First extends Component {
    render() {
        return (
            <div>
                <h1>I am  { this.props.name }</h1>
                <p> Email : { this.props.email }</p>
            </div>
        ) 
    }
}


export default First
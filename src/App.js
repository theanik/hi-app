import React from 'react';
import './App.css';


import First from './component/First'
import Counter from './component/Counter'
import Boom from './component/Boom'
import Hello from './component/Hello'
class App extends React.Component {
  state = {
    persons : [
      {name:"Anik", email : "anik@anw.aa"},
      {name:"Mona", email : "mona@anw.aa"}
    ],
    display : 'none'

  }

  render(){
        return (
          <div className="App">
            {/* this is exaple for passing props data */}
          {/* <First name="Anik Anwar"  email="anik@aa.aa"></First>
          <First name="Brin Adams"  email="anik@bb.aa"></First> */}

          {/* {this.state.persons.map( (item, index) => {
            return <First key={index} name={item.name} email={ item.email} ></First>
          }) } */}

          {/* <Counter></Counter> */}


          {/* <button onClick={this.state.display == 'none' ? this.fun1 : this.fun2}>
            BOOM
          </button>

          <h1 style={{display : this.state.display }}>BOOOOOOOOM</h1> */}

          {/* <Boom /> */}
          <Hello />
          </div>
      );
  }

}

export default App;
